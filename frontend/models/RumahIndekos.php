<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "rumah_indekos".
 *
 * @property int $ID_RUMAHINDEKOS
 * @property int $ID_VENDOR
 * @property string $NAMA_RUMAHINDEKOS
 * @property int $BIAYA
 * @property string $ALAMAT_RUMAHINDEKOS
 * @property string $JENIS_FASILITAS
 * @property int $STOK
 * @property string $GAMBAR
 * @property string $KETERANGAN
 *
 * @property Pemesanan[] $pemesanans
 * @property Vendor $vENDOR
 * @property Ulasan[] $ulasans
 */
class RumahIndekos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rumah_indekos';
    }

    public $file;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_VENDOR'], 'required'],
            [['ID_VENDOR', 'BIAYA', 'STOK'], 'integer'],
            [['NAMA_RUMAHINDEKOS'], 'string', 'max' => 32],
            [['ALAMAT_RUMAHINDEKOS'], 'string', 'max' => 64],
            [['JENIS_FASILITAS', 'KETERANGAN'], 'string', 'max' => 2048],
            [['KETERANGAN'], 'string', 'max' => 2048],
            [['GAMBAR'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['ID_VENDOR'], 'exist', 'skipOnError' => true, 'targetClass' => Vendor::className(), 'targetAttribute' => ['ID_VENDOR' => 'ID_VENDOR']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_RUMAHINDEKOS' => 'Id Rumah Indekos',
            'ID_VENDOR' => 'Id Vendor',
            'NAMA_RUMAHINDEKOS' => 'Nama Rumah Indekos',
            'BIAYA' => 'Biaya/bulan (Rp)',
            'ALAMAT_RUMAHINDEKOS' => 'Alamat Rumah Indekos',
            'JENIS_FASILITAS' => 'Jenis Fasilitas',
            'STOK' => 'Stok',
            'GAMBAR' => 'Gambar',
            'KETERANGAN' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPemesanans()
    {
        return $this->hasMany(Pemesanan::className(), ['id_rumahindekos' => 'ID_RUMAHINDEKOS']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVENDOR()
    {
        return $this->hasOne(Vendor::className(), ['ID_VENDOR' => 'ID_VENDOR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUlasans()
    {
        return $this->hasMany(Ulasan::className(), ['ID_RUMAHINDEKOS' => 'ID_RUMAHINDEKOS']);
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->GAMBAR->saveAs('image/' . $this->GAMBAR->baseName . '.' . $this->GAMBAR->extension);
            return true;
        } else {
            return false;
        }
    }
}
