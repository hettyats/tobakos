<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Pemesanan;

/**
 * PemesananSearch represents the model behind the search form of `frontend\models\Pemesanan`.
 */
class PemesananSearch extends Pemesanan
{
    public $namaKos;
    public $namaCustomer;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namaKos', 'namaCustomer', 'id_pemesanan', 'id_rumahindekos', 'id_customer', 'jumlah_kamar', 'id_status'], 'integer'],
            [['tanggal', 'waktu'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pemesanan::find();
        $query->joinWith(['rumahindekos']);
        $query->joinWith(['customer']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // add conditions that should always apply here

        $dataProvider->sort->attributes['namaKos']=[
            'asc' => ['rumah_indekos.NAMA_RUMAHINDEKOS' => SORT_ASC],
            'desc' => ['rumah_indekos.NAMA_RUMAHINDEKOS' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['namaCustomer']=[
            'asc' => ['customer.NAMA_CUSTOMER' => SORT_ASC],
            'desc' => ['customer.NAMA_CUSTOMER' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pemesanan' => $this->id_pemesanan,
            'id_rumahindekos' => $this->id_rumahindekos,
            'tanggal' => $this->tanggal,
            'waktu' => $this->waktu,
            'jumlah_kamar' => $this->jumlah_kamar,
            'id_status' => $this->id_status,
        ]);

        $query->andFilterWhere(['like', 'rumah_indekos.NAMA_RUMAHINDEKOS', $this->namaKos])
        ->andFilterWhere(['like', 'customer.NAMA_CUSTOMER', $this->namaCustomer]);

        return $dataProvider;
    }
}
