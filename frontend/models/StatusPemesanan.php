<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "status_pemesanan".
 *
 * @property int $id
 * @property string $status
 *
 * @property Pemesanan[] $pemesanans
 */
class StatusPemesanan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status_pemesanan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_pemesanan'], 'required'],
            [['status_pemesanan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_status' => 'ID status',
            'status_pemesanan' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPemesanans()
    {
        return $this->hasMany(Pemesanan::className(), ['id_status' => 'id_status']);
    }
}
