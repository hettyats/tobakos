<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use frontend\models\User;

// $session = Yii::$app->session;
// $user = User::find()->where(['username' => Yii::$app->user->identity->username])->one();
 
$user = User::findOne(Yii::$app->user->id);

// var_dump($user);
// die();

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\Search\RumahIndekosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rumah Indekos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rumah-indekos-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php       
        if($user['role'] == 10){?>
            <p>
                <?= Html::a('Tambah Rumah Indekos', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
    <?php
        }
    ?>
    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <style>
        img{
            width:100px;
            height:100px;
        }
    </style>

    <?php
    if($user != null){
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
    
                // 'ID_RUMAHINDEKOS',
                // 'ID_VENDOR',
                'NAMA_RUMAHINDEKOS',
                // 'BIAYA',
                [
                    'attribute' => 'BIAYA',
                    'value' => function($model){
                        return number_format($model->BIAYA);
                    }
                ],
                'ALAMAT_RUMAHINDEKOS',
                'JENIS_FASILITAS',
                'STOK',
                
                [
                    'attribute' => 'GAMBAR',
                    'format' => 'raw',
                    'label' => 'Gambar',
                    'value' => function($data){
                        // return '<img src="../image/$data->GAMBAR" width="100px" height="100px">';
                        return ($data->GAMBAR) ? Html::img("../image/" . $data->GAMBAR) : false;
                    }
                ],
    
                'KETERANGAN',
    
                [
                    // 'class' => 'common\components\ToolsColumn', 
                    'class' => 'yii\grid\ActionColumn',
                'header' =>  'Actions',
                'template' => '{view} {update} {booking} {ulasan}',
                'buttons' => [
                    'update' => function($url, $model){
                        $user = User::find()->where(['username' => Yii::$app->user->identity->username])->one();
                        if($user['role'] == 10){    
                        return Html::a('<span class="glyphicon glyphicon-edit"></span>', Url::to(['update', 'id' => $model->ID_RUMAHINDEKOS]));
                        }
                    },
                    'del' => function ($url, $model){
                        return "<li>".Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', $url, [
                            'title' => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Apakah anda yakin menghapus item ini?'),
                            'data-post' => 'post',
                            'data-pjax' => '0',
                        ])."</li>";
                    },
                    'view' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['view', 'id' => $model->ID_RUMAHINDEKOS]));
                    },
                    'booking' => function($url, $model){
                        $user = User::find()->where(['username' => Yii::$app->user->identity->username])->one();
                        if($user['role'] == 20){    
                            return Html::a('<span class="glyphicon glyphicon-bed"></span>', Url::to(['pemesanan/create', 'id' => $model->ID_RUMAHINDEKOS]));
                        }
                    },
                    'ulasan' => function($url, $model){
                        $user = User::find()->where(['username' => Yii::$app->user->identity->username])->one();
                        if($user['role'] == 20){   
                        return Html::a('<span class="glyphicon glyphicon-comment"></span>', Url::to(['ulasan/create', 'id' => $model->ID_RUMAHINDEKOS]));
                    }
                }
                ],
                // 'urlCreator' => function ($action, $model, $key, $index){
                //     if ($action === 'view'){
                //         return Url::toRoute(['view', 'id' => $key]);
                //     }
                //     if ($action === 'pesan') {
                //         $userId = Yii::$app->user->getId();
    
                //         $idUser = (new \yii\db\Query())
                //               ->select(['ID_CUSTOMER'])
                //               ->from('CUSTOMER')
                //               ->where(['ID_AKUN' => $userId])
                //                 ->all();
    
                //         $id = $idUser;
    
                //         return Url::toRoute(['pemesanan/create', 'id' => $model->ID_RUMAHINDEKOS]);
                //     }
                //     if ($action === 'testi') {
                //         $userId = Yii::$app->user->getId();
    
                //         $idUser = (new \yii\db\Query())
                //               ->select(['ID_CUSTOMER'])
                //               ->from('CUSTOMER')
                //               ->where(['ID_AKUN' => $userId])
                //                 ->all();
    
                //         $id = $idUser;
    
                //         return Url::toRoute(['ulasan/create', 'id' => $model->ID_RUMAHINDEKOS]);
                //     }
                //     // if ($action === 'edit'){
                //     //     return Url::toRoute(['edit', 'id' => $key]);
                //     // }
                //     // if ($action === 'del'){
                //     //     return Url::toRoute(['del', 'id' => $key]);
                //     // }
                //     },
                ],
            ],
        ]);
    }else{
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
    
                // 'ID_RUMAHINDEKOS',
                // 'ID_VENDOR',
                'NAMA_RUMAHINDEKOS',
                // 'BIAYA',
                [
                    'attribute' => 'BIAYA',
                    'value' => function($model){
                        return number_format($model->BIAYA);
                    }
                ],
                'ALAMAT_RUMAHINDEKOS',
                'JENIS_FASILITAS',
                'STOK',
                
                [
                    'attribute' => 'GAMBAR',
                    'format' => 'raw',
                    'label' => 'Gambar',
                    'value' => function($data){
                        // return '<img src="../image/$data->GAMBAR" width="100px" height="100px">';
                        return ($data->GAMBAR) ? Html::img("../image/" . $data->GAMBAR) : false;
                    }
                ],
    
                'KETERANGAN',
            ],
        ]);
    }
     ?>


</div>
