<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model frontend\models\RumahIndekos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rumah-indekos-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php
        // $id = $_GET['id'];
    ?>
 
    <?= $form->field($model, 'NAMA_RUMAHINDEKOS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'BIAYA')->textInput() ?>

    <?= $form->field($model, 'ALAMAT_RUMAHINDEKOS')->textInput(['maxlength' => true]) ?>
    
    <?php echo $form->field($model, 'JENIS_FASILITAS[]')->checkboxList(
			['Tempat tidur' => 'Tempat tidur ', ' Lemari' => 'Lemari ', 'Kipas Angin' => 'Kipas Angin ', 'AC' => 'AC ', 'Kamar Mandi Dalam' => 'Kamar Mandi Dalam ', 'Sofa' => 'Sofa ', 'Dapur' => 'Dapur ', 'Meja Belajar' => 'Meja Belajar ', 'Dispenser' => 'Dispenser ', 'Wi-Fi' => 'Wi-Fi ', 'Kompor' => 'Kompor ', 'Pantry' => 'Pantry ',]
   	);
    ?>
    
    <?= $form->field($model, 'STOK')->textInput() ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <?= $form->field($model, 'KETERANGAN')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
