<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\RumahIndekos */

$this->title = $model->NAMA_RUMAHINDEKOS;
$this->params['breadcrumbs'][] = ['label' => 'Rumah Indekos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rumah-indekos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID_RUMAHINDEKOS], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID_RUMAHINDEKOS], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Apakah anda yakin menghapus item ini?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->

    <!-- <style>
        img{
            width:100px;
            height:100px;
        }
    </style> -->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'ID_RUMAHINDEKOS',
            // 'ID_VENDOR',
            // 'NAMA_RUMAHINDEKOS',
            [
                'attribute' => 'BIAYA',
                'label' => 'Biaya/bulan (Rp)',
                'value' => function($model){
                    return number_format($model->BIAYA);
                }
            ],
            'ALAMAT_RUMAHINDEKOS',
            'JENIS_FASILITAS',
            'STOK',
            [
                'attribute' => 'GAMBAR',
                'format' => 'raw',
                'label' => 'Gambar',
                'value' => function($data){
                    // return '<img src="../image/$data->GAMBAR" width="100px" height="100px">';
                    return ($data->GAMBAR) ? Html::img("../image/" . $data->GAMBAR) : false;
                }
            ],
            'KETERANGAN',
        ],
    ]) ?>

</div>
