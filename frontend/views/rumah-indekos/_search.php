<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Search\RumahIndekosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rumah-indekos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_RUMAHINDEKOS') ?>

    <?= $form->field($model, 'ID_VENDOR') ?>

    <?= $form->field($model, 'NAMA_RUMAHINDEKOS') ?>

    <?= $form->field($model, 'BIAYA') ?>

    <?= $form->field($model, 'ALAMAT_RUMAHINDEKOS') ?>

    <?php // echo $form->field($model, 'JENIS_FASILITAS') ?>

    <?php // echo $form->field($model, 'STOK') ?>

    <?php // echo $form->field($model, 'GAMBAR') ?>

    <?php // echo $form->field($model, 'KETERANGAN') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
