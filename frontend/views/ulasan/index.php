<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\UlasanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ulasan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ulasan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- <?= Html::a('Create Ulasan', ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'ID_ULASAN',
            [
                'label' => 'Customer',
                'attribute' => 'ID_CUSTOMER',
                'value' => 'customer.NAMA_CUSTOMER',
            ],
            [
                'label' => 'Nama Rumah Indekos',
                'attribute' => 'ID_RUMAHINDEKOS',
                'value' => 'rumahindekos.NAMA_RUMAHINDEKOS',
            ],
            'KOMENTAR',
            // 'RATING',
            [
                'attribute' => 'RATING',
                'format'=>'html',
                'value' => function($model){
                    $counter = $model->RATING;
                    $text = '';
                    for($i=0;$i<5;$i++){
                        if($i < $counter){
                            $text = $text.Html::tag('span', '', ['class' => 'glyphicon glyphicon-star']);
                        }else{
                            $text = $text.Html::tag('span', '', ['class' => 'glyphicon glyphicon-star-empty']);
                        }
                        
                    }
                    
                    return $text;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
