<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ulasan */

$this->title = 'Ulasan';
$this->params['breadcrumbs'][] = ['label' => 'Ulasan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ulasan-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
        'rumah_indekos' => $rumah_indekos,
    ]) ?>

</div>
