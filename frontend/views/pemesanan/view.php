<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\Customer;
use frontend\models\User;
/* @var $this yii\web\View */
/* @var $model frontend\models\Pemesanan */

$session = Yii::$app->session;
$user = User::find()->where(['username' => Yii::$app->user->identity->username])->one();

$this->title = $model->id_pemesanan;
$this->params['breadcrumbs'][] = ['label' => 'Pemesanan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pemesanan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_pemesanan',
            'id_rumahindekos',
            'id_customer',
            'tanggal',
            'jumlah_kamar',
            // [
            //     'attribute' => 'no_telp_Customer',
            //     'label' => 'No. Telepon Customer',
            //     'value' => 'customer.NO_TELP_CUSTOMER',
            // ],
           'id_status'
            
        ],
    ]) ?>
 <?php  
if($user['role'] == 20){?>
    <p>
       <?= Html::a('Cancel', ['delete', 'id' => $model->id_pemesanan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Apakah anda yakin membatalkan pemesanan anda?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
        }
    ?>

</div>
