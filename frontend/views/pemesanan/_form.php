<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datetimepicker\DateTimePicker;
use frontend\models\RumahIndekos;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pemesanan */
/* @var $form yii\widgets\ActiveForm */

$datetime = new DateTime();
$datetime->modify('+2 day');
// $dim = Dim::find()->where('deleted != 1')->andWhere(['user_id' => Yii::$app->user->identity->user_id])->one();

$id = $_GET['id'];
$rumah_indekos = RumahIndekos::findOne($id);

// var_dump($rumah_indekos->STOK);

?>

<div class="pemesanan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        $id = $_GET['id'];
    ?>
    <div class="" style="padding-left:30%; padding-right:30%;" >
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $form->field($model, 'id_rumahindekos')->hiddenInput(['value'=> $id])->label(false);?>
        
        <?= $form->field($model, 'tanggal')->widget(DateTimePicker::className(), [
            'language' => 'en',
            'size' => 'ms',
            'pickButtonIcon' => 'glyphicon glyphicon-time',
            'inline' => false,
            'clientOptions' => [
                'pickerPosition' => 'bottom-left',
                'autoclose' => true,
                'format' => 'yyyy-mm-dd', // if inline = false
                // 'todayBtn' => true,
                'startDate' => date($datetime->format("Y-m-d")),
            ]
        ]);?>

    

    <!-- <?= $form->field($model, 'tanggal')->textInput() ?> -->

    <!-- <?= $form->field($model, 'waktu')->textInput() ?>  -->

    <?= $form->field($model, 'jumlah_kamar')->textInput(['type' => 'number', 'max' => $rumah_indekos->STOK]) ?>

    <!-- <?= $form->field($model, 'status')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    </div>
    
    <?php ActiveForm::end(); ?>

</div>
 