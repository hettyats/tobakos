<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use frontend\models\RumahIndekos;
use frontend\models\StatusPemesanan;
use frontend\models\Customer;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\PemesananSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pemesanan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pemesanan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <p>
        <!-- <?= Html::a('Create Pemesanan', ['create'], ['class' => 'btn btn-success']) ?> -->
    </p> 

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
        
            // 'id_pemesanan',
            // 'NAMA_RUMAHINDEKOS',
            [
                'attribute' => 'namaKos',
                'label' => 'Nama Rumah Indekos',
                'filter' => ArrayHelper::map(RumahIndekos::find()->all(), 'ID_RUMAHINDEKOS', 'NAMA_RUMAHINDEKOS'),
                'filterInputOptions' => ['class' => 'form-control', 'id' => 'ID_VENDOR', 'prompt' => 'Semua'],
                'value' => 'rumahindekos.NAMA_RUMAHINDEKOS',
            ],

            [
                'attribute' => 'namaCustomer',
                'label' => 'Nama Customer',
                'value' => 'customer.NAMA_CUSTOMER',
            ],
            'tanggal',
            'jumlah_kamar',
            // [
            //     'attribute' => 'no_telp_Customer',
            //     'label' => 'No. Telepon Customer',
            //     'value' => 'customer.NO_TELP_CUSTOMER',
            // ],
            // 'NO_TELP_CUSTOMER',
            [
                'attribute' => 'id_status',
                'label' => 'Status',
                'filter' => ArrayHelper::map(StatusPemesanan::find()->all(), 'id_status', 'status_pemesanan'),
                'filterInputOptions' => ['class' => 'form-control', 'id' => NULL, 'prompt' => 'ALL'],
                'value' => 'status.status_pemesanan',
            ],


            
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {confirm} {reject}',
                'buttons' => [
                    'confirm' => function($url, $model){
                        return Html::a('', ['pemesanan/confirm', 'id' => $model->id_pemesanan], ['class' => 'glyphicon glyphicon-ok']);
                    },
                    'reject' => function($url, $model){
                        return Html::a('', ['pemesanan/reject', 'id' => $model->id_pemesanan], ['class' => 'glyphicon glyphicon-remove']);
                    }
                ],
            ],
        ],
    ]); ?>


</div>
