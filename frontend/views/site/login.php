<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="container text-center" style="margin-top:50px">
    <!-- <p>Please fill out the following fields to login:</p> -->
        <div class="row">
            <div class="col-md-20" style="padding-left:30%; padding-right:30%;">
            <h1><?= Html::encode($this->title) ?></h1>
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                    <!-- <img class="rounded-circle w-75 mx-auto d-block">
                            <?= Html::img('@web/image/Logo.jpeg',['alt'=>'some', 'class'=>'rounded-circle w-50 mx-auto d-block']);?> -->
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                        <?= $form->field($model, 'password')->passwordInput() ?>

                        <?= $form->field($model, 'rememberMe')->checkbox() ?>

                        <div style="color:#999;margin:1em 0">
                            Jika anda lupa kata sandi anda, anda dapat <?= Html::a('Mengatur ulang kata sandi', ['site/request-password-reset']) ?>.
                            <br>
                            Anda membutuhkan verifikasi email baru? <?= Html::a('Kirim ulang', ['site/resend-verification-email']) ?>
                        </div>

                        <div class="form-group">
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
