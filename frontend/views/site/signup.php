<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\JenisKelamin;

$this->title = 'Registrasi Customer';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="row">
        <div class="" style="padding-left:20%; padding-right:20%;" >
            <h1><?= Html::encode($this->title) ?></h1>

            <p>Isilah formulir dibawah ini untuk mendaftar sebagai Customer:</p>

            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($modelRegister, 'NAMA_CUSTOMER') ?>

                <?= $form->field($modelRegister, 'ALAMAT_CUSTOMER') ?>

                <?= $form->field($modelRegister, 'NO_TELEPON_CUSTOMER') ?>

                <?= $form->field($modelRegister, 'JENIS_KELAMIN')->label('Jenis Kelamin')->dropDownList(
                    ArrayHelper::map(JenisKelamin::find()->all(),'id','jenisKelamin'),
                    ['prompt'=>'Pilih Jenis Kelamin']
                    )
                ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => false]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'repassword')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
