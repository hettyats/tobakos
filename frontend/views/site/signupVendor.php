<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Registrasi Vendor';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="row">
        <div class="" style="padding-left:20%; padding-right:20%;" >
        <h1><?= Html::encode($this->title) ?></h1>

        <p>Isilah formulir dibawah ini untuk mendaftar sebagai Vendor:</p>
    
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($modelRegisterVendor, 'NAMA_VENDOR') ?>

                <?= $form->field($modelRegisterVendor, 'ALAMAT_VENDOR') ?>

                <?= $form->field($modelRegisterVendor, 'NO_TELEPON__VENDOR') ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => false]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'repassword') ->passwordInput()?>

                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
