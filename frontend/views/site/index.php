<?php
use frontend\models\Ulasan;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'TobaKos';
?>
<div class="site-index">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox" >
    <div class="item active">
      <img src="image/Logo1.png" alt="Kost Makmur">
      <!-- <div class="carousel-caption">
        <h2>Kost Makmur</h2>
      </div> -->
    </div>
    <div class="item">
      <img src="image/iklan1.png" alt="Kost Jaya">
      <!-- <div class="carousel-caption">
        <h2>Kost Jaya</h2>
      </div> -->
    </div>
    <div class="item">
      <img src="image/Carousel1.png" alt="Kost Ceria">
      <!-- <div class="carousel-caption">
        <h2>Kost Ceria</h2>
      </div> -->
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-center" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<hr/>
<h3><b>REKOMENDASI</b></h3>
    <div class="body-content">
      <div class="row">
        <?php
          $rumahIndekos = Ulasan::find()
                                  ->select(['AVG(RATING) AS SUMS, ID_RUMAHINDEKOS'])
                                  ->groupBy(['ID_RUMAHINDEKOS'])
                                  ->orderBy(['SUMS' => SORT_DESC])
                                  ->limit(3)
                                  ->all();

          foreach ($rumahIndekos as $row){
      ?>
        <div class="col-lg-4">
          <div class= "row">
            <div class= "col-lg-12 offset-md-1">
            <h5 class= "pull-right"> Rating: <?php 
                echo Yii::$app->formatter->asDecimal($row->SUMS,1);
            ?>/5</h5>
            </div>  
          </div>
          <!-- <a href="#"><i class="fa fa-circle text-success"></i> Rating</a> -->
            <a href = "<?= Url::to(['rumah-indekos/view', 'id' => $row->rumahindekos->ID_RUMAHINDEKOS]) ?>" style="color:#000000">
              <img src="image/<?php echo $row->rumahindekos->GAMBAR;?>" alt="Kost Makmur" width="100%" href="rumah-indekos/view?id=<?= $row->rumahindekos->ID_RUMAHINDEKOS ?>">
              <p> </p>
            </a>
          <div class= "col-lg-8">
          <h4 class="pull-right"><?php echo $row->rumahindekos->NAMA_RUMAHINDEKOS;?>
          </div>
            <!-- <div class= "col-lg-7 offset-md-1">
          <h5 class= "pull-right"> Rating: <?php echo Yii::$app->formatter->asDecimal($row->SUMS,2);?></h5></h4>
          </div> -->           
        </div>
        <?php
        }
        ?>
        </div>
      </div>
    </div>
</div>