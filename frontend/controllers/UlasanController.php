<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Vendor;
use frontend\models\Ulasan;
use frontend\models\RumahIndekos;
use frontend\models\search\UlasanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;

/**
 * UlasanController implements the CRUD actions for Ulasan model.
 */
class UlasanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserCustomer(Yii::$app->user->identity->username);
                        }
                   ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ulasan models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $user = User::findOne(Yii::$app->user->id);
        // $vendor = Vendor::find()->where(['ID_AKUN' => $user->id])->one();
        // // $rumah_indekos = RumahIndekos::find()->where(['ID_VENDOR' => $vendor->ID_VENDOR])->all(); 
        // $ulasan = [];

        // foreach($rumah_indekos as $kost){
        //     $ulasan[] = Ulasan::find()->where(['ID_RUMAHINDEKOS' => $kost->ID_RUMAHINDEKOS])->all();
            // echo "<pre />";
            // var_dump($kost);
            // die();
        //}

        // echo "<pre />";
        // var_dump($ulasan);
        // die();

        // $rumah_indekos = RumahIndekos::find()->where(['ID_VENDOR' => $vendor->ID_VENDOR])->one();

        $searchModel = new UlasanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider = new ActiveDataProvider([
        //     'query' => Ulasan::find()->where(['ID_RUMAHINDEKOS' => $rumah_indekos->ID_RUMAHINDEKOS])->orderBy('ID_ULASAN asc'),
        // ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ulasan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ulasan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Ulasan();

        $rumah_indekos = RumahIndekos::findOne($id);

        if ($model->load(Yii::$app->request->post())) {

            $userId = Yii::$app->user->getId();

            $id = (new \yii\db\Query())
                  ->select(['ID_CUSTOMER'])
                  ->from('CUSTOMER')
                  ->where(['ID_AKUN' => $userId])
                  ->all();


            // echo '<pre />';
            // print_r($id);
            // die;

            $model->ID_CUSTOMER = $id[0]['ID_CUSTOMER'];

            // $model->ID_RUMAHINDEKOS = $id;


            // echo '<pre>';
            // print_r($model);
            // echo '</pre>';
            // die();


            $model->save();

            return $this->redirect(['view', 'id' => $model->ID_ULASAN]);
        }

        return $this->render('create', [
            'model' => $model,
            'rumah_indekos' => $rumah_indekos,
        ]);
    }

    /**
     * Updates an existing Ulasan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID_ULASAN]);
        }

        return $this->render('update', [
            'model' => $model,
            'rumah_indekos' => $model,
        ]);
    }

    /**
     * Deletes an existing Ulasan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ulasan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ulasan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ulasan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Halaman yang anda minta tidak ditemukan.');
    }
}
