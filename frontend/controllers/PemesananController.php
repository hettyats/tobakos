<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Pemesanan;
use frontend\models\Vendor;
use frontend\models\RumahIndekos;
use frontend\models\search\PemesananSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use yii\data\ActiveDataProvider;

/**
 * PemesananController implements the CRUD actions for Pemesanan model.
 */
class PemesananController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {   
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['confirm', 'reject', 'create'],
                'rules' => [
                    [
                        'actions' => ['confirm', 'reject'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserVendor(Yii::$app->user->identity->username);
                        }
                   ],
                   [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserCustomer(Yii::$app->user->identity->username);
                        }
                ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pemesanan models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $user = User::findOne(Yii::$app->user->id);
        // $vendor = Vendor::find()->where(['ID_AKUN' => $user->id])->one();

        // $rumah_indekos = RumahIndekos::find()->where(['ID_VENDOR' => $vendor->ID_VENDOR])->one();

        // $pemesanan = Pemesanan::find()->where(['id_rumahindekos' => $rumah_indekos->ID_RUMAHINDEKOS])->all();;
        $searchModel = new PemesananSearch();
        
        // $dataProvider = new ActiveDataProvider([
        //     'query' => Pemesanan::find()->where(['id_rumahindekos' => $rumah_indekos->ID_RUMAHINDEKOS])->orderBy('id_pemesanan asc'),
        // ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pemesanan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $pemesanan = Pemesanan::findOne($id);

        return $this->render('view', [
            'model' => $pemesanan,
        ]);
    }

    /**
     * Creates a new Pemesanan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pemesanan();

        if ($model->load(Yii::$app->request->post())) {

            $userId = Yii::$app->user->getId();

            $id = (new \yii\db\Query())
                  ->select(['ID_CUSTOMER'])
                  ->from('CUSTOMER')
                  ->where(['ID_AKUN' => $userId])
                  ->all();

            $model->id_customer = $id;
            $model->tanggal = date('Y-m-d');
            $model->waktu = date('H:i:s');
            $model->id_status = 1;

            // echo '<pre />';
            // print_r($model);
            // die;

            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id_pemesanan]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pemesanan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pemesanan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pemesanan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pemesanan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pemesanan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pemesanan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionCancel($id){
        $pemesanan = Pemesanan::findOne($id);
        $pemesanan->id_status = 1;
        // var_dump($pemesanan->id_status);
        // die();
        // $pemesanan->status = 1;
        $pemesanan->save();

        return $this->actionIndex();
    }

    public function actionConfirm($id){
        $pemesanan = Pemesanan::findOne($id);
        $pemesanan->id_status = 2;
        // var_dump($pemesanan->id_status);
        // die();
        // $pemesanan->status = 2;
        $pemesanan->save();

        return $this->actionIndex();
    }

    public function actionReject($id){
        $pemesanan = Pemesanan::findOne($id);
        $pemesanan->id_status = 3;
        // echo "<pre/>";
        // var_dump($pemesanan);
        // die();
        // $pemesanan->status = 2;
        $pemesanan->save();

        return $this->actionIndex();
    }
}