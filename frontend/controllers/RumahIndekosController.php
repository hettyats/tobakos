<?php

namespace frontend\controllers;

use Yii;
use frontend\models\RumahIndekos;
use frontend\models\Search\RumahIndekosSearch;
use frontend\models\Customer;
use frontend\models\Vendor;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use common\models\User; 
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * RumahIndekosController implements the CRUD actions for RumahIndekos model.
 */
class RumahIndekosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete', 'booking'],
                'rules' => [
                    [
                        'actions' => ['create', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserVendor(Yii::$app->user->identity->username);
                        }
                   ],
                   [
                    'actions' => ['booking'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return User::isUserCustomer(Yii::$app->user->identity->username);
                    }
               ],
                ],
                
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RumahIndekos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RumahIndekosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RumahIndekos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RumahIndekos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RumahIndekos();
        $vendor = Vendor::find()->where(['ID_AKUN' => Yii::$app->user->identity->id])->one();
        if ($model->load(Yii::$app->request->post())) {

            // echo '<pre />';
            // var_dump($model);
            // die();

            $model->ID_VENDOR = $vendor->ID_VENDOR;
            // $model->save();    
            $gambar = $model->ID_RUMAHINDEKOS;
            $image = UploadedFile::getInstance($model, 'file');
            $imageName = 'RumahIndekos_' . $gambar . '.' . $image->getExtension();
            $image->saveAs('img/'.$imageName);
            $model->GAMBAR = $imageName;
            
            
            // echo '<pre />';
            // print_r($imageName);
            // die();

            $model->JENIS_FASILITAS = implode(',', $model->JENIS_FASILITAS);    

            

            $model->save();
            
            return $this->redirect(['view', 'id' => $model->ID_RUMAHINDEKOS]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RumahIndekos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {


            $model->JENIS_FASILITAS = implode(', ', $model->JENIS_FASILITAS);
            $model->GAMBAR = UploadedFile::getInstance($model, 'file');
            $model->GAMBAR->saveAs('img/' . $model->GAMBAR->baseName . '.' . $model->GAMBAR->extension);
           
            // $model->GAMBAR = null; 

            // echo('<pre />');
            // print_r($model);
            // die;

            $model->save(false);
            return $this->redirect(['view', 'id' => $model->ID_RUMAHINDEKOS]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RumahIndekos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RumahIndekos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RumahIndekos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RumahIndekos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
    * Finds the Informasi model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return Informasi the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionUpload()
    {
    $model = new UploadForm();
    if ($model->load(Yii::$app->request->post())) {
        $model->GAMBAR = UploadedFile::getInstances($model, 'GAMBAR');
        if ($model->GAMBAR){
            $file = $model->GAMBAR->name;
            if ($model->GAMBAR->saveAs(Yii::getAlias('@frontend').'/image'.$file)){
                $model->GAMBAR->$file;
            }
        }
        $model->save();
        return $this->redirect(['index']);
    }else {
        return $this->render('upload', ['model' => $model]);
    }
    }
}