/*
SQLyog Community v13.1.1 (64 bit)
MySQL - 10.1.35-MariaDB : Database - tobakos
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tobakos` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tobakos`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `ID_ADMIN` int(11) NOT NULL AUTO_INCREMENT,
  `ID_AKUN` int(11) NOT NULL,
  `NAMA_ADMIN` varchar(32) DEFAULT NULL,
  `ALAMAT_ADMIN` varchar(64) DEFAULT NULL,
  `EMAIL_ADMIN` varchar(32) DEFAULT NULL,
  `NO_TELEPON_ADMIN` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_ADMIN`),
  KEY `ID_AKUN` (`ID_AKUN`),
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`ID_AKUN`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

/*Table structure for table `akun` */

DROP TABLE IF EXISTS `akun`;

CREATE TABLE `akun` (
  `ID_AKUN` varchar(16) NOT NULL,
  `ID_CUSTOMER` varchar(16) NOT NULL,
  `ID_ADMIN` varchar(16) NOT NULL,
  `PERTANYAAN_KEAMANAN` varchar(256) DEFAULT NULL,
  `NAMA_AKUN` varchar(32) DEFAULT NULL,
  `KATA_SANDI` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`ID_AKUN`),
  KEY `FK_akun` (`ID_ADMIN`),
  KEY `FK_akun_customer` (`ID_CUSTOMER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `akun` */

/*Table structure for table `auth_assignment` */

DROP TABLE IF EXISTS `auth_assignment`;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_assignment` */

/*Table structure for table `auth_item` */

DROP TABLE IF EXISTS `auth_item`;

CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_item` */

/*Table structure for table `auth_item_child` */

DROP TABLE IF EXISTS `auth_item_child`;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_item_child` */

/*Table structure for table `auth_rule` */

DROP TABLE IF EXISTS `auth_rule`;

CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_rule` */

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `ID_CUSTOMER` int(11) NOT NULL AUTO_INCREMENT,
  `ID_AKUN` int(11) NOT NULL,
  `NAMA_CUSTOMER` varchar(32) DEFAULT NULL,
  `ALAMAT_CUSTOMER` varchar(64) DEFAULT NULL,
  `NO_TELEPON_CUSTOMER` int(11) DEFAULT NULL,
  `JENIS_KELAMIN` int(11) NOT NULL,
  `EMAIL_CUSTOMER` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID_CUSTOMER`),
  KEY `ID_AKUN` (`ID_AKUN`),
  KEY `JENIS_KELAMIN` (`JENIS_KELAMIN`),
  CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`ID_AKUN`) REFERENCES `user` (`id`),
  CONSTRAINT `customer_ibfk_2` FOREIGN KEY (`JENIS_KELAMIN`) REFERENCES `jenis_kelamin` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `customer` */

insert  into `customer`(`ID_CUSTOMER`,`ID_AKUN`,`NAMA_CUSTOMER`,`ALAMAT_CUSTOMER`,`NO_TELEPON_CUSTOMER`,`JENIS_KELAMIN`,`EMAIL_CUSTOMER`) values 
(1,7,'Hetty','Sitoluama',2147483647,2,'hetty@tobakos.com'),
(2,9,'test','Medan',2147483647,1,'test@test.com'),
(5,11,'Sarah','Balige',0,2,'maria@labalaba.com'),
(6,11,'Jhonson Hutagaol','B',812687237,1,'jhonson@gmail.com'),
(7,13,'samuel','Balige',576568779,1,'samuel@gmail.com'),
(8,13,'Ana','Porsea',2147483647,2,'hettyana.ts@gmail.com'),
(9,13,'Ana','Balige',2147483647,2,'hettyana.ts@gmail.com'),
(10,14,'Marcel','Sipitudai Laguboti',2147483647,1,'marcel@gmai.com'),
(11,15,'Ana','Sitoluama, Laguboti',2147483647,2,'ana@gmail.com'),
(12,16,'Lino','Jalan Hutanagodang, Parmaksian',2147483647,1,'lino@gmail.com'),
(13,24,'celi','jalan suka suka',2147483647,2,'celi@gmail.com'),
(14,24,'Hetty','Sitoluama',2147483647,2,'hetty@tobakos.com');

/*Table structure for table `informasi` */

DROP TABLE IF EXISTS `informasi`;

CREATE TABLE `informasi` (
  `ID_INFORMASI` int(11) NOT NULL AUTO_INCREMENT,
  `ID_RUMAHINDEKOS` int(11) NOT NULL,
  `JENIS_FASILITAS` varchar(32) DEFAULT NULL,
  `STOK` int(11) DEFAULT NULL,
  `KETERANGAN` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`ID_INFORMASI`),
  KEY `FK_informasi_rumah_indekos` (`ID_RUMAHINDEKOS`),
  CONSTRAINT `informasi_ibfk_1` FOREIGN KEY (`ID_RUMAHINDEKOS`) REFERENCES `rumah_indekos` (`ID_RUMAHINDEKOS`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `informasi` */

insert  into `informasi`(`ID_INFORMASI`,`ID_RUMAHINDEKOS`,`JENIS_FASILITAS`,`STOK`,`KETERANGAN`) values 
(1,1,'Kamar memiliki AC',5,'Bersedia ditempati kapan saja');

/*Table structure for table `jenis_kelamin` */

DROP TABLE IF EXISTS `jenis_kelamin`;

CREATE TABLE `jenis_kelamin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenisKelamin` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `jenis_kelamin` */

insert  into `jenis_kelamin`(`id`,`jenisKelamin`) values 
(1,'Laki-laki'),
(2,'Wanita');

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values 
('m000000_000000_base',1557550928),
('m130524_201442_init',1557550931),
('m190124_110200_add_verification_token_column_to_user_table',1557550932);

/*Table structure for table `pemesanan` */

DROP TABLE IF EXISTS `pemesanan`;

CREATE TABLE `pemesanan` (
  `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT,
  `id_rumahindekos` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` time DEFAULT NULL,
  `jumlah_kamar` int(11) unsigned NOT NULL,
  `id_status` int(11) NOT NULL,
  PRIMARY KEY (`id_pemesanan`),
  KEY `id_rumahindekos` (`id_rumahindekos`),
  KEY `id_customer` (`id_customer`),
  KEY `status` (`id_status`),
  CONSTRAINT `pemesanan_ibfk_1` FOREIGN KEY (`id_rumahindekos`) REFERENCES `rumah_indekos` (`ID_RUMAHINDEKOS`),
  CONSTRAINT `pemesanan_ibfk_2` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`ID_CUSTOMER`),
  CONSTRAINT `pemesanan_ibfk_4` FOREIGN KEY (`id_status`) REFERENCES `status_pemesanan` (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `pemesanan` */

insert  into `pemesanan`(`id_pemesanan`,`id_rumahindekos`,`id_customer`,`tanggal`,`waktu`,`jumlah_kamar`,`id_status`) values 
(3,1,1,'2019-05-24','00:00:00',2,3),
(4,1,1,'2019-05-25','00:00:00',1,3),
(5,1,1,'2019-05-25','00:00:00',5,2),
(10,1,1,'2019-05-27','00:00:00',1,3),
(11,1,1,'2019-05-27','00:00:00',1,2),
(12,1,1,'2019-05-28','00:00:00',1,2),
(13,1,1,'2019-05-28','00:00:00',4,2),
(14,1,1,'2019-05-28','00:00:00',4,3),
(15,1,1,'2019-05-28','00:00:00',1,2),
(16,4,1,'2019-05-28','00:00:00',3,3),
(17,1,1,'2019-05-29','05:57:52',1,1),
(20,6,1,'2019-05-29','09:55:59',2,1),
(22,1,1,'2019-06-09','05:33:56',2,1);

/*Table structure for table `rumah_indekos` */

DROP TABLE IF EXISTS `rumah_indekos`;

CREATE TABLE `rumah_indekos` (
  `ID_RUMAHINDEKOS` int(11) NOT NULL AUTO_INCREMENT,
  `ID_VENDOR` int(11) NOT NULL,
  `NAMA_RUMAHINDEKOS` varchar(32) DEFAULT NULL,
  `BIAYA` int(11) DEFAULT NULL,
  `ALAMAT_RUMAHINDEKOS` varchar(64) DEFAULT NULL,
  `JENIS_FASILITAS` varchar(2048) DEFAULT NULL,
  `STOK` int(11) DEFAULT NULL,
  `GAMBAR` varchar(2550) DEFAULT NULL,
  `KETERANGAN` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`ID_RUMAHINDEKOS`),
  KEY `FK_rumah_indekos_vendor` (`ID_VENDOR`),
  CONSTRAINT `rumah_indekos_ibfk_1` FOREIGN KEY (`ID_VENDOR`) REFERENCES `vendor` (`ID_VENDOR`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `rumah_indekos` */

insert  into `rumah_indekos`(`ID_RUMAHINDEKOS`,`ID_VENDOR`,`NAMA_RUMAHINDEKOS`,`BIAYA`,`ALAMAT_RUMAHINDEKOS`,`JENIS_FASILITAS`,`STOK`,`GAMBAR`,`KETERANGAN`) values 
(1,2,'Kost Jaya',1600000,'Laguboti','Tempat tidur,  Lemari, Kipas Angin',3,'kos_2.jpg','Bisa pasutri'),
(2,2,'Kost Makmur',2000000,'Sitoluama','Tempat tidur,  Lemari',3,'RumahIndekos_.jpg','Bisa Pasutri'),
(3,2,'Kost Ceria',1000000,'Jalan Ceria','Kipas Angin, Kamar Mandi Dalam, Sofa',3,'Rumahindekos3.jpg','Bisa Pasutri'),
(4,2,'Kost Suka-suka',130000,'Sipiongot',' Bed,  Lemari',3,'RumahIndekos_.jpg','Sekamar berdua'),
(5,4,'Kost Bersih',300000,'Jalan Air Bersih, Sitoluama','Tempat tidur, AC, Kamar Mandi Dalam, Sofa',5,'kost_1.jpg','Sekamar berdua'),
(6,4,'Kost Indah',2000000,'Jalan Sukma','AC,Kamar Mandi Dalam,Sofa',3,'RumahIndekos_.jpg','Sekamar bertiga'),
(7,2,'Kost Aman',120000,'Jalan Aman, Sitoluama','Tempat tidur,Kamar Mandi Dalam,Dapur',3,'RumahIndekos_.jpg','Sekamar bertiga');

/*Table structure for table `status_pemesanan` */

DROP TABLE IF EXISTS `status_pemesanan`;

CREATE TABLE `status_pemesanan` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `status_pemesanan` varchar(255) NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `status_pemesanan` */

insert  into `status_pemesanan`(`id_status`,`status_pemesanan`) values 
(1,'Menunggu konfirmasi'),
(2,'Disetujui'),
(3,'Ditolak');

/*Table structure for table `ulasan` */

DROP TABLE IF EXISTS `ulasan`;

CREATE TABLE `ulasan` (
  `ID_ULASAN` int(11) NOT NULL AUTO_INCREMENT,
  `ID_CUSTOMER` int(11) NOT NULL,
  `ID_RUMAHINDEKOS` int(11) NOT NULL,
  `KOMENTAR` varchar(128) DEFAULT NULL,
  `RATING` int(1) DEFAULT NULL,
  PRIMARY KEY (`ID_ULASAN`),
  KEY `FK_ulasan_customer` (`ID_CUSTOMER`),
  KEY `FK_ulasan_rumah_indekos` (`ID_RUMAHINDEKOS`),
  CONSTRAINT `ulasan_ibfk_1` FOREIGN KEY (`ID_CUSTOMER`) REFERENCES `customer` (`ID_CUSTOMER`),
  CONSTRAINT `ulasan_ibfk_3` FOREIGN KEY (`ID_RUMAHINDEKOS`) REFERENCES `rumah_indekos` (`ID_RUMAHINDEKOS`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `ulasan` */

insert  into `ulasan`(`ID_ULASAN`,`ID_CUSTOMER`,`ID_RUMAHINDEKOS`,`KOMENTAR`,`RATING`) values 
(8,1,2,'AirBersih',3),
(9,1,3,'AirBersih. Recomended',5),
(10,1,3,'Nyaman',4),
(11,1,1,'Nyaman',2),
(12,1,4,'Nice',4),
(13,1,1,'wenak',5),
(14,1,1,'AA',5),
(15,1,2,'asd',5),
(17,1,4,'Nyaman',4),
(18,1,5,'nyaman',4),
(19,1,6,'Bersih',5),
(20,1,5,'Sejuk',4),
(21,1,1,'berhasil',4);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`role`,`status`,`created_at`,`updated_at`,`verification_token`) values 
(2,'admin','fAq80jGvqlt-7a_CI6m2w1bi-joql0V5','$2y$13$d9X4BLDM3f7rt2HijOqmV.nWoi88iB4yQ9WxWK5wfZZti0SnS03zC',NULL,'admin@test.com',30,9,1557581671,1557581671,'qmzVFEg7g9GtjxeGxNwAP1HP-krKpLT1_1557581671'),
(3,'user','YcaoZJ731SYMRLPu-s4wov7S2cJp7O9g','$2y$13$MV8lpvxddQ9FCIOrppBO0ePpTt7SjvA4kQuXU6s/qn4WNvmWQwZlS',NULL,'user@test.com',0,9,1557582801,1557582801,'zIlu223cMcNVVayZlflb4qWPjJcjHiWc_1557582801'),
(4,'vendor','6w8y3O50gnFVUrTHlXm2QudoHfJuEpD2','$2y$13$RSWWpLtQp.inLL1ieQP08uCCqUtShrpwrujGkQXY/SFyxva47z8RW',NULL,'vendor@test.com',0,9,1557582838,1557582838,'VBxFICzpWVpjyMnbIb5IDIj5wqHOyBuC_1557582838'),
(7,'hetty','Qyol5B94C2Ug2jnZ5JLdMWh7sbVBYumt','$2y$13$Oi93QqDwNoT1pqgDOPRLQeZRLdAvyCCrUJ2elcruDKUJE9JJgMofq',NULL,'hetty@tobakos.com',20,9,1557597480,1557597480,'RbkiUrRYCNlXGRELYFyGRRPqyiq5-9Lv_1557597480'),
(8,'budi','5Rqn_AdcXZkvC0OZ6SR2_fw5kUxE72q0','$2y$13$AesegUb1x/HNRTmQYU/f/emDPKuQbvhwKrEuy.BgVZCnOn9882IB6',NULL,'budi@tobakos.com',0,9,1557599199,1557599199,'ji-FcNGAWbHFNm07OPkRanwnlmjl9456_1557599199'),
(9,'test','KtCN4N3LUzpqzJfv10jKV9EKoqOdIXxu','$2y$13$rCW6U3SSnCKg2zgPc/.8w.3Pcz1Dn6Gz1awqV1DZZuue1T9X.yIEq',NULL,'test@test.com',0,9,1557646509,1557646509,'QoaABSCrOAFM-5ULy304NCaIW1EiOOcI_1557646509'),
(10,'mariaa','q6RxHLPA01OgkFBzoljSXI_4uauNjsHo','$2y$13$tDmiJrvQYJzoNynv5PYfm.0Gx9mW.JY9ZQREhgLe1BpJ9/39FphSO','ThQQK0q4fr6HPILzR3CUkr0z3dJ4U2Gd_1558758569','maria@labalaba.com',10,9,1558691146,1558758569,'hiWrk-RIrUnwH_jcSoNf5CEj-sGLvqXy_1558691146'),
(11,'cika','1y39OaWco5uyCFSU6QatSMRmIEt_KAXi','$2y$13$vtwqQgixph1zdUoXnbH1n.JWXrtlfnIfkWtQyGocgbo.AQKCb9xfa','tA_1t6-fn-f4-mtd2_NuWVZg4TarWS2V_1558694724','franciskahutagaol66@gmail.com',0,9,1558691255,1558694724,'RPhnsAnDxq0qWaKhOGDkHpRLqYCLWOBB_1558691255'),
(12,'steve','sn4y30_TATn0nWreTmMleaDkTF2kFCJP','$2y$13$xnb4AzrzlwW9ANY269zBlO2kSbVgqGENbm6fOlUXK2ls1aK81k6uW',NULL,'steve@gmail.com',20,9,1558968181,1558968181,'RnkpO3-is55vTJGN3l7R7e02iXGR2gmE_1558968181'),
(13,'samuel','Ii5o2SxmMU_kGJ-vdjuTVDzHNDraZOri','$2y$13$vZMoQ8j8VE/3ykM5aLo5pOzcJKzZuGBiiOeN7Z1Dr.WSuXb5dTCWq',NULL,'samuel@gmail.com',20,9,1558968315,1558968315,'KTEHZmTcNFKigm-GmUgp3yL921SJO_JP_1558968315'),
(14,'marcel','jGHpvXWvRpyCvcIODUqpbCaxGuQZfLjJ','$2y$13$MsuuQudBToK8.W9kz9mEBOlD0dCqzX1W2bMYd48PSOPKeIZm9vOqG',NULL,'marcel@gmai.com',20,9,1559098888,1559098888,'1WsdC5I1xLAVsRmA8kDYrkfi63msAgH7_1559098888'),
(15,'anaa','0kJqyqT1N6UohiDOUAd8IGNXYE4-4ogj','$2y$13$yPS6BDvfzILEAPrLRgW9LO4HPCmfNhnQml2HAybi0qhhqJ4cOrjGy',NULL,'ana@gmail.com',20,9,1559106364,1559106364,'Smhi0_GCPOhDkREbHRNlSCf2nOvSMqpX_1559106364'),
(16,'lino','YA2ayYSE-Rz440FeU0DXBMa_fJ-_Fxgm','$2y$13$8Lwmqg6E7jeVmj1gSBhKleUAUHd2jNIlsIhWbf4R.7A6qfhjLzrE.',NULL,'lino@gmail.com',20,9,1559106873,1559106873,'PbA5CwZzEG6prD_H0zgiQAaGHG3DyrZf_1559106873'),
(17,'thasya','1RKm-mQNRlSc8LSWbgbcXeYmOcDSeWeO','$2y$13$OlR3Cf6PTE4XEYqV6cl83eRLUA3LJVU2vpatfdFOZx/u5D6RHwTyG',NULL,'thasya@gmail.com',10,9,1559107111,1559107111,'n8uGIW5zcWQJni8geHmAbqfGRGMI3pue_1559107111'),
(18,'leo','M5O0ZkgONWxs593f49h8hQ82rXUvlwnm','$2y$13$0yvcxjLWzkdAZJXOUH70OOOlPbn3RFPASSqTClDGe6zPh4nomkTMa',NULL,'leonaldo@gmail.com',10,9,1559108104,1559108104,'QS9jOXvCqUhfHFd05Rim1z9SKTXBwnoY_1559108104'),
(19,'qwerty','FUOVJ62rsLwSQcif1xpCQQXhcwZB5dhe','$2y$13$QBPu9aGYhP9osS2EVyxjI.qVm8nC0BaRlu8LXkbDnbcILwbwsmMLy',NULL,'qwerty@gmail.com',10,9,1559108843,1559108843,'T1FcEZfbjt26_762NEe9teIc1YJu-6Jn_1559108843'),
(20,'jose','TcMXDzVH0Bw58CPzqJIlBDvH5diVIlF0','$2y$13$/BDWBI58OZbNi8eOKX/RyOia2XjvibmSG7zfJiIbUgiXR1shMbr9a',NULL,'jose@gmail.com',10,9,1559109131,1559109131,'aVdktCKtRM1e6Vr9pAfLLG7x-hMf1Jx9_1559109131'),
(23,'hettyats','z71WkpoNYuuTBi07ROHi3AB6MuiTHrDQ','$2y$13$7fyt2uVW2nzkz5em5mrvJeZMuIIArvhHObKzjMrgrGIHzwhkuIXay',NULL,'hettyats@gmail.com',10,9,1559111814,1559111814,'oDKoz6aWJGrgWcYDuJNEHVz_SZiHpA8n_1559111814'),
(24,'celi','gsM9z0AethAqnEDyV73-1dY7imMyLySF','$2y$13$oO6EvS232.d0.qjZmTzbuufOeVX9r9yJyrounsA923BB22nY1Cy9C',NULL,'celi@gmail.com',20,9,1559118370,1559118370,'rVFjp0sdhRYTtwsXliMh5v05XfdQudrG_1559118370');

/*Table structure for table `vendor` */

DROP TABLE IF EXISTS `vendor`;

CREATE TABLE `vendor` (
  `ID_VENDOR` int(11) NOT NULL AUTO_INCREMENT,
  `ID_AKUN` int(11) NOT NULL,
  `NAMA_VENDOR` varchar(32) DEFAULT NULL,
  `ALAMAT_VENDOR` varchar(64) DEFAULT NULL,
  `EMAIL_VENDOR` varchar(32) DEFAULT NULL,
  `NO_TELEPON__VENDOR` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_VENDOR`),
  KEY `vendor` (`ID_AKUN`),
  CONSTRAINT `vendor_ibfk_1` FOREIGN KEY (`ID_AKUN`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `vendor` */

insert  into `vendor`(`ID_VENDOR`,`ID_AKUN`,`NAMA_VENDOR`,`ALAMAT_VENDOR`,`EMAIL_VENDOR`,`NO_TELEPON__VENDOR`) values 
(1,8,'Budi','Balige','budi@tobakos.com',2147483647),
(2,10,'Maria','Sipiongot','maria@labalaba.com',2147483647),
(3,9,'lenovo','del','lenovo@gmail.com',2147483647),
(4,23,'hettyats','Jalan Hutanagodang','hettyats@gmail.com',2147483647),
(5,10,'Maria','Sipiongot','maria@labalaba.com',2147483647),
(6,10,'Maria','Sipiongot','maria@labalaba.com',2147483647);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
